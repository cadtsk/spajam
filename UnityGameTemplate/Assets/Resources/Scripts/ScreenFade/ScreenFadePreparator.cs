﻿using UnityEngine;
using System.Collections;

public class ScreenFadePreparator : MonoBehaviour {

	void Awake () {
		if (GameObject.FindWithTag ("ScreenFadeManager") == null) {
			GameObject screenFadeManager = Instantiate (Resources.Load ("Prefabs/ScreenFadeManager")) as GameObject;
			screenFadeManager.name = screenFadeManager.name.Replace ("(Clone)", "");
		}
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
