﻿using UnityEngine;
using System.Collections;

public class ScreenFader : MonoBehaviour {

	// 各イベント
	public event ScreenFadeManager.OnFade onBeforeFadeOut;
	public event ScreenFadeManager.OnFade onAfterFadeOut;
	public event ScreenFadeManager.OnFade onBeforeFadeIn;
	public event ScreenFadeManager.OnFade onAfterFadeIn;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartFade () {
		// フェードを司るオブジェクトをタグ検索
		ScreenFadeManager fadeManager = GameObject.FindWithTag ("ScreenFadeManager").GetComponent<ScreenFadeManager> ();
		// フェード情報の登録
		fadeManager.RegisterateFadeInfo (GetComponent<FadeInfo> ());
		// イベントの登録
		fadeManager.onBeforeFadeOut += this.onBeforeFadeOut;
		fadeManager.onAfterFadeOut += this.onAfterFadeOut;
		fadeManager.onBeforeFadeIn += this.onBeforeFadeIn;
		fadeManager.onAfterFadeIn += this.onAfterFadeIn;
		// フェード開始
		fadeManager.StartFade ();
	}
}
