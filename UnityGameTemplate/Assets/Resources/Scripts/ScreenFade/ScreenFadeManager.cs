﻿using UnityEngine;
using System.Collections;

/// <summary>
/// シーン遷移時のフェードイン・アウトを制御するためのクラス
/// </summary>
public class ScreenFadeManager : MonoBehaviour {
	/// <summary>暗転用黒テクスチャ</summary>
	private Texture2D fadeTexture;
	/// <summary>フェード中かどうか</summary>
	private bool isFading = false;
	/// <summary>フェード時に必要な情報</summary>
	private FadeInfo fadeInfo = null;

	// フェード途中に処理をはさみたい場合eventに処理を登録する
	public delegate void OnFade ();
	public event OnFade onAfterFadeOut;
	public event OnFade onBeforeFadeOut;
	public event OnFade onAfterFadeIn;
	public event OnFade onBeforeFadeIn;

	public void Awake () {
		// 全てのシーンでこのオブジェクトを使いまわす
		DontDestroyOnLoad (this.gameObject);

		// ここでスクリーンフェード用テクスチャを作る
		this.fadeTexture = new Texture2D (1, 1, TextureFormat.RGB24, false);
		this.fadeTexture.ReadPixels (new Rect (0, 0, 1, 1), 0, 0, false);
		this.fadeTexture.SetPixel (0, 0, Color.white);
		this.fadeTexture.Apply ();
	}

	/// <summary>
	/// フェード情報を登録する関数
	/// </summary>
	public void RegisterateFadeInfo (FadeInfo fadeInfo) {
		this.fadeInfo = fadeInfo;
	}

	/// <summary>
	/// フェードを開始する関数
	/// </summary>
	public void StartFade () {
		StartCoroutine ("Fade");
	}

	/// <summary>
	/// シーン遷移用コルーチン
	/// </summary>
	private IEnumerator Fade () {
		// フェード情報が空っぽだった場合、処理を中断する
		if (this.fadeInfo == null) {
			// エラーログ表示
			Debug.Log ("フェードに必要な情報が登録されていません");
			yield break;
		}

		// シーンを変更しようとしているのにシーン名が指定されていない場合、処理を中断する
		if (this.fadeInfo.UsesChangeScene) {
			if (this.fadeInfo.NextSceneName == "") {
				// エラーログ表示
				Debug.Log ("シーンが指定されていません");
				yield break;
			}
		}

		// フェード開始
		this.isFading = true;

		// フェード開始の通知
		Debug.Log ("Start Fade");

		// フェード中はタッチが反応しないよう、Canvasにフィルターを配置
		GameObject beforeSceneFilter = Instantiate (Resources.Load ("Prefabs/TouchFilter")) as GameObject;
		beforeSceneFilter.name = beforeSceneFilter.name.Replace ("(Clone)", "");
		beforeSceneFilter.transform.SetParent (GameObject.FindWithTag ("UICanvas").transform, false);

		// フェードアウト前の処理
		if (onBeforeFadeOut != null) {
			onBeforeFadeOut ();
		}

		// フェードアウト
		float time = 0;
		while (time < this.fadeInfo.FadeOutTime_secs) {
			this.fadeInfo.FadeColor = new Color (
				this.fadeInfo.FadeColor.r,
				this.fadeInfo.FadeColor.g,
				this.fadeInfo.FadeColor.b,
				Mathf.Lerp (0f, 1f, time / this.fadeInfo.FadeOutTime_secs));
			time += Time.deltaTime;
			yield return null;
		}

		// フェードアウト後の処理
		if (onAfterFadeOut != null) {
			onAfterFadeOut ();
		}

		// フェードアウトが終了したのでフィルターを排除
		Destroy (beforeSceneFilter.gameObject);

		// シーン切替
		if (this.fadeInfo.UsesChangeScene) {
			Application.LoadLevel (this.fadeInfo.NextSceneName);
		}

		// シーン切り替え後、1フレーム待機
		yield return null;

		// フェード中はタッチが反応しないよう、Canvasにフィルターを配置
		GameObject afterSceneFilter = Instantiate (Resources.Load ("Prefabs/TouchFilter")) as GameObject;
		afterSceneFilter.name = afterSceneFilter.name.Replace ("(Clone)", "");
		afterSceneFilter.transform.SetParent (GameObject.FindWithTag ("UICanvas").transform, false);

		// 画面が完全に隠れた状態で指定時間待機
		yield return new WaitForSeconds (this.fadeInfo.WaitTime_secs);

		// フェードイン前の処理
		if (onBeforeFadeIn != null) {
			onBeforeFadeIn ();
		}

		// フェードイン
		time = 0;
		while (time < this.fadeInfo.FadeInTime_secs) {
			this.fadeInfo.FadeColor = new Color (
				this.fadeInfo.FadeColor.r,
				this.fadeInfo.FadeColor.g,
				this.fadeInfo.FadeColor.b,
				Mathf.Lerp (1f, 0f, time / this.fadeInfo.FadeOutTime_secs));
			time += Time.deltaTime;
			yield return null;
		}

		// フェードイン後の処理
		if (onAfterFadeIn != null) {
			onAfterFadeIn ();
		}

		// フェードインが終了したのでフィルターを排除
		Destroy (afterSceneFilter.gameObject);

		// 次のフェードで影響が及ばないよう、イベントの中身を空にする
		onBeforeFadeIn = null;
		onBeforeFadeOut = null;
		onAfterFadeIn = null;
		onAfterFadeOut = null;

		// フェードを終了
		this.isFading = false;
	}

	public void OnGUI () {
		if (!this.isFading)
			return;

		// 透明度を更新してテクスチャを描画
		GUI.color = this.fadeInfo.FadeColor;
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), this.fadeTexture);
	}
}